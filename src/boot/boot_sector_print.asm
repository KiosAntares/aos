print:
    pusha   ; push registers

wstart:
    mov al, [bx]    ; deref. string address
    cmp al, 0       ; eos
    je wdone        ; string ended

    mov ah, 0x0e    ; tty mode
    int 0x10        ; print
    
    inc bx          ; move to next character
    jmp wstart      ; loop

wdone:
    popa    ; pop registers
    ret


print_nl:
    pusha   
    
    mov ah, 0x0e
    mov al, 0x0a
    int 0x10
    mov al, 0x0d
    int 0x10
    
    popa
    ret
