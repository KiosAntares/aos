#define COLOR 0x02

#define UDIV_UP(a,b) (((a) + (b) - 1) / (b))
#define ALIGN_UP(a,b) (UDIV_UP(a,b) * (b))


void dummy_test_entrypoint() {
}

void clear(char * vm){
    int i;
    for(i = 0; i<10*80*2;i+=2)
        *(vm+i) = ' ';
}

void print(char* vm, char* str){
    int i = 0;
    while(str[i] != 0){
        *vm = str[i];
        *(vm+1) = COLOR;
        i++;
        vm += 2;
    }
}



void main() {
    char* video_memory = (char*) 0xb8000;
    int a = 2;
    char* str = "Hello kernel";
   
    clear(video_memory); 
    print(video_memory, str);    
}
